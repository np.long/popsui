const getPageData = async () => {
    const req = new request();
    req.setParams({
        v: '1.1',
        code: 'home',
        page: '1',
        limit: '5'
    });
    const res = await req.get('/Pages/components');
    return res.data;
}

