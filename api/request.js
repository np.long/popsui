const REQUEST_METHOD = {
  GET: 'GET',
  POST: 'POST'
}

const request = function() {
  const endPoint = 'https://products.popsww.com/api/v5';
  let headers = {
    'profileid': '5f6ef181040877002b2b1a16',
    'lang': 'vi',
    'platform': 'web',
    'api-key': '5d2300c2c69d24a09cf5b09b',
    'authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjoiYmQ3NTk2NTEwMzlmNjM3MDZkY2I3NDI4ZmQ4NTdmYzdmMDM5NjcwYWJiYzVjMTU5Y2ExMDJkZmJmMDMwYmQxMGEzZGI3YTM0ZTlmNzc2MjEzZjVhZWI4ZGVhNjI1MjYzNTYzZDgzMzY4NzFiZjQ3ODVjOTFiNWI4NzY2MWU1MTMyNDA4NzVmYTRmMzFlNzA3NDk0YTA2NmY4MjA4NTY3NDA5OGUxZmMyYzVkZTIwM2YwOTI3N2EzYzAyZDNhZTlmNTI1ZTQ3NjNkZDY2MDgzOWU5YmY4MmEwYjdhMzE3MDEzZmJjNDk5YzAzODU1NTY1ODEzZmU0M2Q2ZTA2Mzk0YyIsImlhdCI6MTYwMTEwNjMwNiwiZXhwIjoxOTE2NDY2MzA2fQ.67uzdqMVJgmJzBKNG7aYm8LUVOHLNmdIY-6R4U48c-g'
  };
  let params = null;

  const convertToXHRParams = (obj) => {
    let str = '';
    Object.keys(obj).map(key => {
      str += str ? `&${key}=${obj[key]}` : `${key}=${obj[key]}`;
    });
    return str;
  } 

  const XHRCall = (method, url) => new Promise((resolved, rejected) => {
    var xhr = new XMLHttpRequest();
    if(params && method === REQUEST_METHOD.GET) {
      url += `?${convertToXHRParams(params)}`
    }
    xhr.open(method, `${endPoint}${url}`, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    if(headers) {
      const headersKeys = Object.keys(headers);
      headersKeys.forEach(key => {
        xhr.setRequestHeader(key, headers[key]);
      });
    }

    xhr.send(method === REQUEST_METHOD.POST ? params : null);
   
    xhr.onreadystatechange = function () {
      if (this.readyState === 4) {
        const res = JSON.parse(this.responseText);
        if (this.status < 400) {
          return resolved(res)
        } else {
          return rejected(res)
        }
      }
    };

  });

  this.setHeaders = (keyValueObj) => {
    Object.assign(headers, keyValueObj);
  }

  this.setParams = (paramsObject) => {
    params = paramsObject;
  }

  this.get = async (url) => {
    return await XHRCall(REQUEST_METHOD.GET, url);
  }

  this.post = async (url) => {
    return await XHRCall(REQUEST_METHOD.POST, url);
  }

}
