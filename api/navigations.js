const getNavData = async () => {
    const req = new request();
    req.setParams({
      profileType: 'adult'
    });
    const res = await req.get('/Navigations/getNavigationV2a');
    return res.data;
}