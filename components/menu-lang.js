const renderMenuLang = () => {
    const containLang = document.getElementById("menuLanguage");
    const menuLang = `
        <button class="menu__language__btn" id="menuLanguageBtn" onClick="onLanguageClick()">
            <img src="images/menu/vietnam.png" alt="vi" />
            <span>VI</span>
        </button>
        <div class="menu__language__panel" id="menuLanguagePanel">
            <div class="menu__language__panel__select">
                <img src="images/menu/uk.png" alt="en" />
                <span>EN</span>
            </div>
            <div class="menu__language__panel__select">
                <img src="images/menu/vietnam.png" alt="vi" />
                <span>VI</span>
            </div>
        </div>
    `
    containLang.innerHTML = menuLang;

}


const onLanguageClick = () => {
    var btnLanguage = document.getElementById("menuLanguageBtn");
    var panelLanguage = document.getElementById('menuLanguagePanel');
    if (panelLanguage.classList.contains('show')) {
        panelLanguage.classList.remove('show');
    } else {
        panelLanguage.classList.add('show');
    }
    if (btnLanguage.classList.contains('turn')) {
        btnLanguage.classList.remove("turn");
    } else {
        btnLanguage.classList.add('turn');
    }
}