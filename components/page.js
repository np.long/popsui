const renderPage = async () => {
    const data = await getPageData();
    renderSlider(data);
    renderSeries(data);
    // renderPlaylist(data);

}