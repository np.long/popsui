const renderSeries = (data) => {
    const seriesItem = data.find(item => {
        return item.type === 'topic'
    })

    var containSeries = document.getElementById("series");
    const seriesHeading = `
        <h2 class="series__heading">
            <span class="series__heading__text">${seriesItem.title}</span>
            <div>
                <button class="series__heading__btn">
                    <img src="images/hot-series/icon-back.svg" alt="Previous" />
                </button>
                <button class="series__heading__btn">
                    <img src="images/hot-series/icon-next.svg" alt="Previous" />
                </button>
            </div>
        </h2>
    `
    const seriesItems = seriesItem?.items.map((item) => {
        return ` 
            <div class="series__list__item">
                <a>
                    <img src="${item.thumbnail}" />
                </a>
            </div>  
        `
    })
    
    containSeries.innerHTML = seriesHeading + `<div class="series__list">${seriesItems.join('')}</div>`
}