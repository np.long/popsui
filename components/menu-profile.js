const renderProfileMenu = () =>{
  const containAvt = document.getElementById("menuAvatar");
  const menuAvt = `
    <button class="menu__avatar__btn" onClick="menuProfileClick()">
      <img src="images/menu/avt01.png" alt="avt01" width="40" />
    </button>
    <div class="menu__avatar__panel" id="menuProfilePanel">
      <div class="menu__avatar__panel__contain">
        <div class="menu__avatar__panel__contain__avt01">
          <img src="images/menu/avt01.png" alt="avt01" />
          <div>Người Lớn</div>
        </div>
      <div class="menu__avatar__panel__contain__kids">
        <img src="images/menu/kids01.png" alt="avt01" width="60" />
        <div>Trẻ Em</div>
      </div>
    </div>
    <div class="menu__avatar__panel__submit">
      <div class="menu__avatar__panel__submit__text">
        <a href="https://pops.vn/top-up">Nạp Stars</a>
      </div>
      <div class="menu__avatar__panel__submit__text">
        <a href="https://pops.vn/settings/remove-ads">Mua gói</a>
      </div>
      <div class="menu__avatar__panel__submit__text">
        <a href="https://pops.vn/settings">Tải video</a>
      </div>
      <div class="menu__avatar__panel__submit__text">
        <a href="https://pops.vn/settings">Cài đặt</a>
      </div>
      <hr />
    </div>
    <div class="menu__avatar__panel__login">
      <div><a>Đăng nhập</a></div>
    </div>
  </div>
  `
  containAvt.innerHTML = menuAvt;
}

const menuProfileClick = () => {
  var menuAvatarPanel = document.getElementById("menuProfilePanel");
  if (menuAvatarPanel.classList.contains('show')) {
    menuAvatarPanel.classList.remove('show');
  } else {
    menuAvatarPanel.classList.add('show');
  }
}