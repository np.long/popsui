const autoSwitchSlider = (maxLength) => {
  var index = 0;
  setInterval(() => {
    index++;
    if(index >= maxLength) {
      index = 0;
    }
    switchSlider(index);
  }, 1000);
}

const switchSlider = (index) => {
  const activeClass = 'active';
  var dotElements = document.getElementsByClassName("slider__dots__item");
  var sliderElements = document.getElementsByClassName("slider__item");

  Array.from(dotElements).forEach((el, i) => {
    if (el.classList.contains(activeClass)) {
      el.classList.remove(activeClass)
    }
    if (i === index) {
      el.classList.add(activeClass);
    }
  });

  Array.from(sliderElements).forEach((el, i) => {
    if (el.classList.contains(activeClass)) {
      el.classList.remove(activeClass)
    }
    if (i === index) {
      el.classList.add(activeClass);
    }
  });
}

const renderDots = (items) => {
  var dotsContainer = document.getElementById('sliderDots');
  const dotsItems = items.map((item, index) => {
    return `
        <li class="slider__dots__item" onClick="switchSlider(${index})"></li>
      `
  })
  dotsContainer.innerHTML += dotsItems.join('');
}

const renderSliderItems = (items) => {
  var container = document.getElementById('slider');
  const sliderItems = items.map((item) => {
    const date = new Date(item.releaseAt)
    return `
        <div class="slider__item">
          <div class="slider__item__info">
            <h2>${item.titleImage ?
              `<img src="${item.titleImage}"/>`: item.title}           
            </h2>   
            <p>
              Năm phát hành: <span>${date.getFullYear()}</span>
            </p>
            <a href="#"> Detail </a>
          </div>
          <img src="${item.bannerURL}" alt="${item.bannerURL}"/>
        </div>
      `
  })
  container.innerHTML += sliderItems.join('');
}

const renderSlider = (data) => {
  const sliderItem = data.find(item => {
    return item.type === 'slider'
  })

  renderSliderItems(sliderItem?.items);
  renderDots(sliderItem?.items);
  autoSwitchSlider(sliderItem?.items?.length || 0);
}