const renderMainMenu = async () => {
    const navData = await getNavData();
    const container = document.getElementById("navContainer");

    const adultItems = navData.filter(item => {
        return item?.mpaa?.type === 'adult' && item.type === 'page'
    })

    const menuItems = adultItems.map((item) => {
        return `
            <li><a href="#">${item.title}</a></li>
        `
    })
    container.innerHTML += menuItems.join('');
}