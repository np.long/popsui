const renderMenuSearch = () => {
  const containSearch = document.getElementById("menuSearch");
  const menuSearch = `
    <input class="menu__search__txtSearch" id="txtSearch" type="text" placeholder="Tìm kiếm">
    <input class="menu__search__btnSearch" type="submit" value="" onClick="menuSearchClick()">
  `
  containSearch.innerHTML = menuSearch;
}

const menuSearchClick = () => {
  var txtSearch = document.getElementById("txtSearch");
  if (txtSearch.classList.contains('active')) {
    txtSearch.classList.remove('active');
  } else {
    txtSearch.classList.add('active');
  }
}