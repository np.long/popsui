window.addEventListener('load', async () => {

  renderMainMenu();
  renderMenuLang();
  renderProfileMenu();
  renderMenuSearch();
  renderPage();
  
})